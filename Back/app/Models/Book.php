<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    public function createBook( $request){
        $this->name = $request->name;
        $this->price = $request->price;
        $this->pages = $request->pages;
        $this->category = $request->category;
        $this->author = $request->author;
        $this->condition = $request->condition;
        $this->user_id = $request->user_id;
        $this->save();
    }

    public function updateBook( $request,$id){
        if($request->name){
            $this->name = $request->name;
        }
        if($request->price){
            $this->price = $request->price;
        }
        if($request->pages){
            $this->pages = $request->pages;
        }
        if($request->category){
            $this->category = $request->category;
        }
        if($request->author){
            $this->author = $request->author;
        }
        if($request->condition){
            $this->condition = $request->condition;
        }
        if($request->user_id){
            $this->user_id = $request->user_id;
        }
        $this->save();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}

