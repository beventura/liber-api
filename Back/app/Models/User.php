<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    public function createUser( $request){
        $this->name = $request->name;
        $this->email = $request->email;
        $this->cpf = $request->cpf;
        $this->adress = $request->adress;
        $this->cep = $request->cep;
        $this->tel_number = $request->tel_number;
        $this->password = $request->password;
        $this->save();
    }

    public function updateUser( $request,$id){
        if($request->name){
            $this->name = $request->name;
        }
        if($request->email){
            $this->email = $request->email;
        }
        if($request->cpf){
            $this->cpf = $request->cpf;
        }
        if($request->adress){
            $this->adress = $request->adress;
        }
        if($request->cep){
            $this->cep = $request->cep;
        }
        if($request->condition){
            $this->condition = $request->condition;
        }
        if($request->password){
            $this->password = $request->password;
        }
        $this->save(); 
    }

    public function books(){
        return $this->hasMany('App\Models\Book');
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
