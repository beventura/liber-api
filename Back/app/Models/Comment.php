<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    
    public function createComment( $request){
        $this->text = $request->text;
        $this->date = $request->date;
        $this->user_id = $request->user_id;
        $this->book_id = $request->book_id;
        $this->save();
        
    }

    public function updateComment( $request,$id){
        if($request->text){
            $this->text = $request->text;
        }
        if($request->date){
            $this->date = $request->date;
        }
        if($request->user_id){
            $this->user_id = $request->user_id;
        }
        if($request->book_id){
            $this->book_id = $request->book_id;
        }
        $this->save();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function book(){
        return $this->belongsTo('App\Models\Book');
    }


    
}
