<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function create(Request $request){
        $comment = new Comment;
        $comment->createComment($request);
        $comment->save();
        return response()->json(['comment'=> $comment], 200);
    }

    public function index(){
        $comment = Comment::all();
        return response()->json(['comment'=> $comment], 200);
    }

    public function show($id){
        $comment = Comment::findOrFail($id);
        return response()->json(['comment'=> $comment],200);
    }

    public function update(Request $request,$id){
        $comment = Comment::findOrFail($id);
        $comment->updateComment($request,$id);
        return response()->json(['comment'=> $comment],200);
    }

    public function delete($id){
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return response()->json(['Comentário deletado do banco de dados!'],200);
    }
}
