<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function create(Request $request){
        $user = new User;
        $user->createUser($request);
        $user->save();
        return response()->json(['user'=> $user], 200);
    }

    public function index(){
        $user = User::all();
        return response()->json(['user'=> $user], 200);
    }

    public function show($id){
        $user = User::findOrFail($id);
        return response()->json(['user'=> $user],200);
    }

    public function update(Request $request,$id){
        $user = User::findOrFail($id);
        $user->updateUser($request,$id);
        return response()->json(['user'=> $user],200);
    }

    public function delete($id){
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json(['Usuário deletado do banco de dados!'],200);
    }

    
    
}
