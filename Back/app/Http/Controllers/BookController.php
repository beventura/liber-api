<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    public function create(Request $request){
        $book = new Book;
        $book->createBook($request);
        $book->save();
        return response()->json(['book'=> $book], 200);
    }

    public function index(){
        $book = Book::all();
        return response()->json(['book'=> $book],200);
    }

    public function show($id){
        $book = Book::findOrFail($id);
        return response()->json(['book'=> $book],200);
    }

    public function update(Request $request,$id){
        $book = Book::findOrFail($id);
        $book->updateBook($request,$id);
        return response()->json(['book'=> $book],200);

    }

    public function delete($id){
        $book = Book::findOrFail($id);
        $book->delete($id);
        return response()->json(['Livro deletado do banco de dados!'],200);
    }


    




}
