import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  registerForm: FormGroup;
  
  constructor(public formbuilder: FormBuilder) {
    this.registerForm = this.formbuilder.group({
      name:[null],
      email:[null],
      emailVal:[null],
      password:[null],
      passwordVal:[null]
    });
   }
  
   submitForm(form) {
    console.log(form);
    console.log(form.value);
  }
  ngOnInit() {}

}
