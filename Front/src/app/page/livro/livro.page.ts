import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

class Livros{
  title: string;
  img: string;
  subtitle:string;
  author: string;
  price: number;
  rating:number;
  description:string;
  category: string[];
}

@Component({
  selector: 'app-livro',
  templateUrl: './livro.page.html',
  styleUrls: ['./livro.page.scss'],
})
export class LivroPage implements OnInit {

  

  livros: Livros[];
  iconeFavoritos: string = "heart-outline";
  
 
  
  constructor(public alertController: AlertController, public toastController: ToastController) {}

  async alerta() {
    const alert = await this.alertController.create({
      header: 'CONFIRMAR COMPRA',
      message: 'Tem certeza que deseja realizar essa compra?',
      buttons: [
        {
        text:"Sim",
        handler: () => {
          this.toastSim();
        }
      },
      {
        text:"Não",
        handler: () => {
          this.toastNao();
        }
      }]
    });
    await alert.present();
  }

  async toastSim(){
    const toast = await this.toastController.create({
      message: 'Compra realizada com sucesso',
      duration: 3000
    })
    toast.present();
  }

  async toastNao(){
    const toast = await this.toastController.create({
      message: 'Compra cancelada',
      duration: 3000
    })
    toast.present();
  }

   
  adicionarFavorito(livro:string): string{
    if(this.iconeFavoritos === "heart-outline"){
      this.iconeFavoritos = "heart";
      this.toastFav();
    }
    else{
      this.iconeFavoritos = "heart-outline";
      this.toastRemove();
    }
    console.log(this.iconeFavoritos);
    return 'Livro adicionado com sucesso';
  }

  async toastFav(){
    const toast = await this.toastController.create({
      message: 'Livro adicionado a sua lista de favoritos!',
      duration: 2000
    })
    toast.present();
  }

  async toastRemove(){
    const toast = await this.toastController.create({
      message: 'Livro removido da sua lista de favoritos!',
      duration: 2000
    })
    toast.present();
  }

  

  ngOnInit() {
    this.livros = [ 
      {
        title: 'Sapiens',
        subtitle: 'Uma breve história da humanidade',
        img:"../../assets/icon/sapiens.jpg",
        author: 'Yuval Noah Harari',
        price:40,
        rating: 4.7,
        description:"O que possibilitou ao Homo sapiens subjugar as demais espécies? O que nos torna capazes das mais belas obras de arte, dos avanços científicos mais impensáveis e das mais horripilantes guerras? Nossa capacidade imaginativa. Somos a única espécie que acredita em coisas que não existem na natureza, como Estados, dinheiro e direitos humanos. Partindo dessa ideia, Yuval Noah Harari, doutor em história pela Universidade de Oxford, aborda em Sapiens a história da humanidade sob uma perspectiva...   LER MAIS  ",
        category: [
          'Ciência',
          'História' 
        ] 
      },
      {
        title: 'Percy Jackson',
        subtitle: ' e o Ladrão de raios',
        img:"../../assets/icon/percy.jpg",
        author: 'Rick Riordan',
        price:28,
        rating: 4.8,
        description:"Os cinco livros da série que se tornou fenômeno mundial, em edição limitada e com design exclusivo: as cinco lombadas dos livros compõem, juntas, uma ilustração especial de John Rocco. Em O ladrão de raios, Percy Jackson, o menino que aos doze anos descobre que é um semideus, filho de Poseidon, precisa impedir uma guerra entre os deuses que destruiria a civilização ocidental; em O Mar de Monstros, ele e os amigos se envolvem em uma perigosa aventura para defender o acampamento dos semideuses; em A maldição do titã, Percy descobre que o Senhor dos Titãs despertou e está disposto a destruir a humanidade; em A batalha do Labirinto, o semideus vai combater o perigoso titã no temido Labirinto de Dédalo; e em O último olimpiano, Percy tem que lidar não só com o exército de Cronos, mas também com a chegada de seu décimo sexto aniversário — e, assim, com a profecia que determinará seu destino.",
        category: [
          'Ficção',
          'Aventura',
          'Mitologia' 
      ] 
    }
  ]

  }
}
